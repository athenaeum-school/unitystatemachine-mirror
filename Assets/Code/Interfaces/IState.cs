﻿using UnityEngine;
using System.Collections;

namespace Assets.Code.Interfaces {

	public interface IState {

		// Use this for initialization
		void Render () ;
		
		// Update is called once per frame
		void StateUpdate () ;
	}

}