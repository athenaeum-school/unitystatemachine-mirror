﻿using UnityEngine;
using Assets.Code.Interfaces;

namespace Assets.Code.States {
	public class BeginState : IState { // MonoBehaviorではなく、上記で宣言したIState
		// StateManagerのインスタンスを再利用できるよう準備
		private StateManager manager;
		
		public BeginState(StateManager stateManager) {
			//初期化
			manager = stateManager;
			Time.timeScale = 0;
		}
		
		public void StateUpdate() {
			//更新処理
			if(Input.GetKeyUp(KeyCode.Return)) { 
				// Returnキーを押すとBeginStateに遷移
				//Application.LoadLevel("Scene0");
				Debug.Log ("Switch to PlayState");
				manager.SwitchState(new PlayState(manager));
			}
		}
			
		public void Render() {
				//描画等
			if (GUI.Button (new Rect (5, 5, 150, 100), "Play")) {
				Time.timeScale = 1;
				manager.SwitchState(new PlayState(manager));
			}

		}
	}
			
}