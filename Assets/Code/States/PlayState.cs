﻿using UnityEngine;
using Assets.Code.Interfaces;

namespace Assets.Code.States {
	public class PlayState : IState { // MonoBehaviorではなく、上記で宣言したIState
		// StateManagerのインスタンスを再利用できるよう準備
		private StateManager manager;
		
		public PlayState(StateManager stateManager) {
			//初期化
			manager = stateManager;
		}
		
		public void StateUpdate() {
			//更新処理
			if(Input.GetKeyUp(KeyCode.Return)) { 
				// Returnキーを押すとPlayStateに遷移
				//Application.LoadLevel("Scene1");
				Debug.Log ("Switch to BeginState");
				manager.SwitchState(new BeginState(manager));
			}
		}
			
			// Playボタンを生成して、ボタンが押されたときに、PlayStateに遷移
		public void Render() {
				//描画等
			if(GUI.Button(new Rect(5, 5, 150, 100), "Play")) {
			
				// StateUpdate()の動作と同じなので、メソッドを定義すると読みやすくなります。
				// 時間があれば練習してみてください。
				//Application.LoadLevel("Scene1");
				Debug.Log ("Switch to BeginState");
				manager.SwitchState(new BeginState(manager));    
			}
		}
	}
			
}
